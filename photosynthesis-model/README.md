# Variation of rate represenations of ATP Synthase and Cyt b6f

Alteration of the photosynthesis model by Matuszynska et al. 2019, in order to propose an update that creates a more compatible model version with delta Psi  

## Use

Install [miniconda](https://docs.conda.io/en/latest/miniconda.html).


Create the conda environment from the supplied `environment-3.yml`

`conda env create -f environment-3.yml`

Run notebooks in the `master_thesis/model_changes_josha` folder via jupyter notebook

`jupyter notebook`
